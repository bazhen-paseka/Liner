/**
* \version 1.0
* \author Bazhen Paseka
* \13-July-2022
*************************************************************************************
* \copyright	Bazhen Paseka
* \copyright	Brovary
* \copyright	Ukraine
*************************************************************************************
*/

#ifndef BEKON_LOCAL_CONFIG_SM_H_
#define BEKON_LOCAL_CONFIG_SM_H_

/*
**************************************************************************
*								INCLUDE FILES
**************************************************************************
*/

/*
**************************************************************************
*								    DEFINES                     
**************************************************************************
*/
	#define 	SOFT_VERSION 		710
	#define		LINER_T
#ifndef LINER_T
	#define		LINER_R
#endif

	//#define 	SLAVE
#ifdef SLAVE
	#define		NMEA_EN
	#define		GNSS
#endif

#ifndef SLAVE
//		#define 	REPEATER
//#define 		PRINT_PAYLOAD
#endif

	//#define 	LOW_POWER_STOP_MODE		//	STOP much better then SLEEP
	//#define 		LOW_POWER_SLEEP_MODE
	//#define		FREQUENCY_TEST
	//#define		FREQUENCY_TEST2
	//#define		WWDG_EarlyWakeupEnable
	#define		PRINT_SUM_ALL_REG
	//#define		PRINT_TOTAL_LORA_REG
	//#define 		PRINT_PAYLOAD


	//#define 	BUZZER
	#define 	BUZZER_QNT			5
	#define 	FLASH_QNT			5
	#define 	BATTERY_MIN_LEVEL	310
	#define		MY_DEBUG
#ifdef MY_DEBUG
	#define 	UART_DEBUG			&huart1
#endif

	#define 	LORA_SPI				&hspi2

	//#define		LOCK_CHIP

	//#define 		CAD_PRINT
	//#define 		STOP_PRINT

	//#define		GNSS_ATTEMPT_MAX		  	39UL
	//#define		GNSS_UPDATE_TIME_SEC_MIN	28UL
	//#define		GNSS_UPDATE_TIME_SEC_NORM	30UL

	//#define		GNSS_UPDATE_TIME_MIN_MIN	 0UL
	//#define		GNSS_UPDATE_TIME_MIN_NORM	11UL

	//#define		GNSS_UPDATE_TIME_HOUR_MIN	 0UL
	//#define		GNSS_UPDATE_TIME_HOUR_NORM	 0UL

	#define		DELAY_TO_REPEAT_SEC		2
	#define		DELAY_TO_SCAN_SEC		5
/*
**************************************************************************
*								   DATA TYPES
**************************************************************************
*/

/*
**************************************************************************
*								GLOBAL VARIABLES
**************************************************************************
*/

/*
**************************************************************************
*									 MACRO'S                     
**************************************************************************
*/

/*
**************************************************************************
*                              FUNCTION PROTOTYPES
**************************************************************************
*/

/*
**************************************************************************
*                              		  END
**************************************************************************
*/
#endif /* BEKON_LOCAL_CONFIG_SM_H_ */
