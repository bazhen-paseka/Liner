/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DIO1_Pin GPIO_PIN_13
#define DIO1_GPIO_Port GPIOC
#define DIO1_EXTI_IRQn EXTI4_15_IRQn
#define BUTTON_Pin GPIO_PIN_2
#define BUTTON_GPIO_Port GPIOA
#define BUTTON_EXTI_IRQn EXTI2_3_IRQn
#define LED_Pin GPIO_PIN_3
#define LED_GPIO_Port GPIOA
#define RESET_Pin GPIO_PIN_10
#define RESET_GPIO_Port GPIOB
#define DIO0_Pin GPIO_PIN_11
#define DIO0_GPIO_Port GPIOB
#define DIO0_EXTI_IRQn EXTI4_15_IRQn
#define NSS_Pin GPIO_PIN_12
#define NSS_GPIO_Port GPIOB
#define debug_Tx_Pin GPIO_PIN_9
#define debug_Tx_GPIO_Port GPIOA
#define DIO4_Pin GPIO_PIN_3
#define DIO4_GPIO_Port GPIOB
#define DIO4_EXTI_IRQn EXTI2_3_IRQn
#define DIO3_Pin GPIO_PIN_4
#define DIO3_GPIO_Port GPIOB
#define DIO3_EXTI_IRQn EXTI4_15_IRQn
#define DIO2_Pin GPIO_PIN_9
#define DIO2_GPIO_Port GPIOB
#define DIO2_EXTI_IRQn EXTI4_15_IRQn
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
